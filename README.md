# Message over Avian Carrier

This project exposes a RESTful API to store (in memory) and search messages.

## Implementation details 

Every message is stored in a collection supported random access.
When a message is stored, three indexes (recipient, originator and the date (to the day granularity)) are updated.
Each of these indexes store a reference to the message position in the collection (to avoid data duplication and save
memory). They also allows a fast filtering on these properties.    

## Building and Running

In order to build this project you will need SBT.

### Build
    
Simply run the following command:
```bash
$ sbt compile
```
    
### Test

Simply run the following command:
```bash
$ sbt test
```

### Run

Simply run the following command:
```bash
$ sbt app/run
```
A server will start on 

## Using the API

### Saving a message

Send a `POST` request to  `http://$SERVER_URL/messages` with an application/json body corresponding to a message.

```bash
curl -X POST \
  http://$SERVER_URL/messages \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"recipient": 4,
	"originator": "she",
	"createdAt": "2018-10-16T00:00:00+00:00",
	"message" : "test"
}'
```

### Querying messages

### Searching / Filtering

To query and filter messages, you can perform a `GET` request to `http://$SERVER_URL/messages`.
You can filter/search messages based on their attribute by adding query parameters to the request.
These query parameters should be formatted like the following `field[operator]=value`.
Some operators supports a list of values, which are represented as a comma separated string.
When filtering on multiple attribute, an implicit AND is applied.


| Operators | Description | Supported attributes |
| --- | --- | --- |
| eq | Check for equality | All |  
| ne | Check for non equality | All | 
| in | Check that the attribute is in the provided list of values | All |
| nin | Check that the attribute is not in the provided list of values | All |
| lt | Lower than the given value | All |
| lte | Lower than or equal the given value | All |
| gt | Greater than the given value | All |
| gte | Greater than or equal the given value | All |
| sw | Starts with the given value | originator, message | 
| ew | Ends with the given value | originator, message |
| contains | Contains the given value | originator, message |
| regex | Check if the attribute matches the given regex | originator, message |


Example:

```
curl -X GET \
  'http://$SERVER_URL/messages?recipient[gt]=1&originator[in]=me,she&createdAt[lt]=2018-08-10T15:00:00%2B00:00&message[eq]=test'
```

### Aggregating

To perform an aggregation on the messages, you can perform a `GET` request to `http://$SERVER_URL/messages/aggregate`.
You must specify a query parameter `function` describing the aggregation function you want to use.
In addition to this `function` you can specify another parameter `attr` (recipient, originator, createAt, message) 
to perform the aggregation on.
The last query parameter you can use is `groupBy` a coma separated list of attributes 
(recipient, originator, createAt, message) to partition the messages according to that clause. 

| Function | Description |
| --- | --- | 
| count | Counts the number of messages |
| min | Return the minimum of the given attribute |
| max | Return the maximum of the given attribute |

Example:

```
curl -X GET \
  'http://$SERVER_URL/messages/aggregate?function=min&attr=createdAt&groupBy=recipient,originator'
```

## Limitations

- Special character in the values must be URLEncoded (for instance `+` in the `createdAt` field).
- When using a list of values (for instance with `in`) the delimiter cannot be changed and is not escaped. Therefore
it is currently impossible to match a string with a coma.
- Only the previously mentioned operators and aggregation functions are supported.