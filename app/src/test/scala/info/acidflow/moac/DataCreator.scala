package info.acidflow.moac

import java.time.LocalDateTime

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import info.acidflow.moac.json.JsonSupport
import info.acidflow.moac.model.Message
import spray.json._

import scala.concurrent.Future

object DataCreator extends App with JsonSupport {

  implicit val system = ActorSystem("data-creator")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val httpClient = Http()

  val msgs = Seq(
    Message(1, "me", LocalDateTime.now().minusDays(7), "test1"),
    Message(2, "you", LocalDateTime.now().minusDays(5), "test2"),
    Message(3, "she", LocalDateTime.now().minusDays(3), "test3"),
    Message(4, "me", LocalDateTime.now().minusDays(1), "test4"),
    Message(1, "you", LocalDateTime.now(), "test5"),
    Message(2, "me", LocalDateTime.now().minusDays(10), "test6"),
    Message(3, "me", LocalDateTime.now().minusDays(2), "test7"),
    Message(1, "me", LocalDateTime.now().minusDays(4), "test8"),
    Message(4, "me", LocalDateTime.now().minusDays(8), "test9"),
  )

  var msgSS = Seq.empty[Message]
  for(i <- 0 until 1000) {
    msgSS = msgSS ++ msgs
  }



  msgSS.foldLeft(Future.successful()) {
    case (f, msg) => f.flatMap(_ => {
      println(msg.toJson.compactPrint)
      httpClient.singleRequest(
        HttpRequest(
          method = HttpMethods.POST,
          uri = Uri("http://127.0.0.1:8080/messages"),
          entity = HttpEntity(ContentTypes.`application/json`, msg.toJson.compactPrint)
    ))}).map(_ => {})
    }.onComplete(_ => system.terminate())

//  Future.sequence(msgSS.map(msg => {
//    println(msg.toJson.compactPrint)
//          httpClient.singleRequest(
//            HttpRequest(
//              method = HttpMethods.POST,
//              uri = Uri("http://127.0.0.1:8080/messages"),
//              entity = HttpEntity(ContentTypes.`application/json`, msg.toJson.compactPrint)
//        ))}
//  )).onComplete(_ => system.terminate())

}
