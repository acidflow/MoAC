package info.acidflow.moac.service

import java.time.{LocalDate, LocalDateTime}

import info.acidflow.moac.model.{Count, Equal, In, Message}
import info.acidflow.moac.repository.message.{InMemoryMessageRepository, MessageRepository}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

class DefaultMessageServiceTest extends FlatSpec with Matchers with MockFactory {

  behavior of "MessageServiceTest"

  it should "save" in {
    val repository = mock[MessageRepository]
    val msg1 = Message(1, "me", LocalDateTime.now(), "test")
    val msg2 = Message(1, "you", LocalDateTime.now(), "test 2")

    (repository.save _).expects(msg1).returning(0)
    (repository.save _).expects(msg2).returning(1)

    val service = new DefaultMessageService(repository)
    service.save(msg1)
    service.save(msg2)

    service.dateIndex.get(LocalDate.now()) should be(Some(Seq(0, 1)))
    service.originatorIndex.get("me") should be(Some(Seq(0)))
    service.recipientIndex.get(1) should be(Some(Seq(0, 1)))

  }

  it should "return the matching recipients" in {
    val repository = new InMemoryMessageRepository()
    val msg1 = Message(1, "me", LocalDateTime.now(), "test")
    val msg2 = Message(2, "you", LocalDateTime.now(), "test 2")

    val service = new DefaultMessageService(repository)
    service.save(msg1)
    service.save(msg2)

    service.filter(Seq((Equal, "1")), Seq.empty, Seq.empty, Seq.empty) should be(Seq(msg1))
    service.filter(Seq((In, "1,2")), Seq.empty, Seq.empty, Seq.empty) should be(Seq(msg1, msg2))
    service.filter(Seq.empty, Seq.empty, Seq.empty, Seq.empty) should be(Seq(msg1, msg2))
  }

  it should "return count of messages" in {
    val repository = new InMemoryMessageRepository()
    val msg1 = Message(1, "me", LocalDateTime.now(), "test")
    val msg2 = Message(2, "you", LocalDateTime.now(), "test 2")

    val service = new DefaultMessageService(repository)
    service.save(msg1)
    service.save(msg2)

    service.aggregate(Count, None, Seq.empty, Seq.empty, Seq.empty, Seq.empty, Seq.empty) should be(Seq[Map[String, Any]](Map("count" -> 2)))
  }

}
