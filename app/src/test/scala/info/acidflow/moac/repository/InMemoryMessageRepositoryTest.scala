package info.acidflow.moac.repository

import java.time.LocalDateTime

import info.acidflow.moac.model.Message
import info.acidflow.moac.repository.message.InMemoryMessageRepository
import org.scalatest.{FlatSpec, Matchers}

class InMemoryMessageRepositoryTest extends FlatSpec with Matchers {

  behavior of "InMemoryMessageRepository"

  it should "save a message and return its position" in {
    val msg = Message(1, "me", LocalDateTime.now(), "test")
    val repository = new InMemoryMessageRepository()
    val pos = repository.save(msg)

    pos should be (0)
    repository.get(0) should be (msg)
  }

  it should "save two messages and return a sequence of messages" in {
    val msg1 = Message(1, "me", LocalDateTime.now(), "test")
    val msg2 = Message(2, "you", LocalDateTime.now(), "test 2")
    val repository = new InMemoryMessageRepository()
    repository.save(msg1)
    repository.save(msg2)

    repository.get(Seq(0, 1)) should be (Seq(msg1, msg2))
  }

}
