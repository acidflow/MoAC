package info.acidflow.moac.repository.index

import org.scalatest.{FlatSpec, Matchers}

class IndexTest extends FlatSpec with Matchers {

  behavior of "Index"

  it should "create a new list of value when updating a non existing key" in {
    val index = new Index[Int, Int]()

    index.update(0, 1)
    index.get(0) should be (Some(Seq(1)))
  }

  it should "update the list associated to a given key" in {
    val index = new Index[Int, Int]()

    index.update(0, 1)
    index.update(0, 2)
    index.get(0) should be (Some(Seq(1, 2)))
  }

  it should "return None when a key does not exists" in {
    val index = new Index[Int, Int]()

    index.get(0) should be (None)
  }

}
