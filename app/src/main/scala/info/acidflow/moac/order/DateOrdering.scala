package info.acidflow.moac.order

import java.time.{LocalDate, LocalDateTime}

/**
  * Ordering for java.time API.
  */
object DateOrdering {

  object Implicits {

    implicit val javaLocalDateOrdering: Ordering[LocalDate] = Ordering.fromLessThan((d1, d2) => d1.isBefore(d2))
    implicit val javaLocalDateTimeOrdering: Ordering[LocalDateTime] = Ordering.fromLessThan((d1, d2) => d1.isBefore(d2))

  }

}
