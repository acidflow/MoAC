package info.acidflow.moac.repository.message

import info.acidflow.moac.model.Message

trait MessageRepository {

  def save(msg : Message) : Int

  def get(positions : Seq[Int]) : Seq[Message]

  def get(position : Int) : Message

  def all() : Iterable[Message]

}
