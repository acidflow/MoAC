package info.acidflow.moac.repository.message

import info.acidflow.moac.model.Message

import scala.collection._

/**
  * Store messages in memory using a collection supporting random access.
  * This allows to build indexes referencing a position in the collection and accessing it in constant time.
  */
class InMemoryMessageRepository extends MessageRepository {

  private val store = new mutable.ArrayBuffer[Message]()

  override def save(msg: Message): Int = synchronized {
    store += msg

    store.length - 1
  }

  override def get(positions: Seq[Int]): Seq[Message] = {
    positions.map(store)
  }

  override def get(position: Int): Message = store(position)

  override def all(): Seq[Message] = store
}
