package info.acidflow.moac.repository.index

import scala.collection.{SortedMap, mutable}

class Index[K: Ordering, V] {

  val index = new mutable.TreeMap[K, mutable.Seq[V]]

  def update(key: K, value: V): Unit = {
    val updatedValues = index.get(key)
      .map(_ :+ value)
      .getOrElse(mutable.Seq[V](value))

    index += (key -> updatedValues)
  }

  def get(key: K): Option[Seq[V]] = {
    index.get(key)
  }

  def filterKeys(predicate : K => Boolean): SortedMap[K, mutable.Seq[V]] = index.filterKeys(predicate)

}