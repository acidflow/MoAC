package info.acidflow.moac

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import info.acidflow.moac.json.{JsonSupport, UnmarshallerSupport}
import info.acidflow.moac.model._
import info.acidflow.moac.service.DefaultMessageService

class MessageController(val messageService: DefaultMessageService) extends UnmarshallerSupport with JsonSupport {

  val route: Route = pathPrefix("messages") {
    get {
      parameterMap { params =>
        // Filter properties
        val filters = generateFilters(params.filterKeys(isMessageAttr))

        val recipientFilters = filters.getOrElse(MessageAttrs.recipient, Seq.empty)
        val originatorFilters = filters.getOrElse(MessageAttrs.originator, Seq.empty)
        val createdAtFilters = filters.getOrElse(MessageAttrs.createdAt, Seq.empty)
        val messageFilters = filters.getOrElse(MessageAttrs.message, Seq.empty)

        path("aggregate") {
          parameters('function, 'attr.?, 'groupBy.?) { (function, attr, groupBy) =>
            complete {
              messageService.aggregate(
                AggregateFunctions(function),
                attr,
                groupBy.map(_.split(",").toSeq).getOrElse(Seq.empty),
                recipientFilters, originatorFilters, createdAtFilters, messageFilters
              )
            }
          }
        } ~ {
          complete {
            messageService.filter(recipientFilters, originatorFilters, createdAtFilters, messageFilters)
          }
        }
      }
    } ~
      post {
        entity(as[Message]) { message =>
          messageService.save(message)
          complete(StatusCodes.Created, message)
        }
      }
  }

  private def isMessageAttr(str: String) = {
    MessageAttrs.All.exists(str.startsWith)
  }

  private def parseOperator(key: String) = {
    val regex = """([a-zA-Z]+)\[([a-z]+)\]""".r
    val parsed = regex.findFirstMatchIn(key).map(m => (m.group(1), m.group(2)))
    require(parsed.isDefined, s"Malformed filter $key")
    val (property, operator) = parsed.get

    (property, Operators(operator))
  }

  private def generateFilters(filters: Map[String, String]) = {
    filters.foldLeft(Map.empty[String, Seq[(Operator, String)]]) {
      case (map, (k, v)) =>
        val (prop, op) = parseOperator(k)
        map + (prop -> (map.getOrElse(prop, Seq.empty) :+ (op, v)))
    }
  }

}
