package info.acidflow.moac.json

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.unmarshalling.Unmarshaller

trait UnmarshallerSupport {

  implicit val localDateTimeUnmarshaller = Unmarshaller.strict[String, LocalDateTime](str => {
    LocalDateTime.parse(str, DateTimeFormatter.ISO_DATE_TIME)
  })

}
