package info.acidflow.moac.service

import java.time.LocalDate

import info.acidflow.moac.model._
import info.acidflow.moac.order.DateOrdering.Implicits._
import info.acidflow.moac.repository.index.Index
import info.acidflow.moac.repository.message.MessageRepository

class DefaultMessageService(val msgRepository: MessageRepository) extends MessageService {

  val recipientIndex = new Index[Long, Int]()
  val originatorIndex = new Index[String, Int]()
  val dateIndex = new Index[LocalDate, Int]()

  override def save(msg: Message): Unit = synchronized {
    val position = msgRepository.save(msg)

    recipientIndex.update(msg.recipient, position)
    originatorIndex.update(msg.originator, position)
    dateIndex.update(msg.createdAt.toLocalDate, position)
  }

  override def aggregate(function: AggregateFunction, prop: Option[String], groupBy: Seq[String] = Seq.empty,
                recipientsFilters: Seq[(Operator, String)], originatorFilter: Seq[(Operator, String)],
                createdAtFilter: Seq[(Operator, String)], messageFilters: Seq[(Operator, String)]): Iterable[Map[String, Any]] = {
    val messages = filter(recipientsFilters, originatorFilter, createdAtFilter, messageFilters)

    function.aggregate(groupBy, prop, messages)
  }

  override def filter(recipientsFilters: Seq[(Operator, String)], originatorFilter: Seq[(Operator, String)],
             createdAtFilter: Seq[(Operator, String)], messageFilters: Seq[(Operator, String)]): Iterable[Message] = {
    import info.acidflow.moac.model.Filter._

    val recipientSet = emptySeqAsOption(recipientsFilters).map(recipientsFilters =>
      recipientIndex.filterKeys(k => recipientsFilters.forall { case (op, values) =>
        LongFilter.call(op, values, k)
      }).values.flatten.toSet
    )

    val originatorSet = emptySeqAsOption(originatorFilter).map(originatorFilter =>
      originatorIndex.filterKeys(k => originatorFilter.forall { case (op, values) =>
        StringFilter.call(op, values, k)
      }).values.flatten.toSet
    )

    val timeHorizonSet = emptySeqAsOption(createdAtFilter).map(createdAtFilter =>
      dateIndex.filterKeys(k => createdAtFilter.forall {
        case (op, values) =>
          LocalDateFilter.call(op, values, k)
      }).values.flatten.filter(pos => {
        val createdAt = msgRepository.get(pos).createdAt
        createdAtFilter.forall { case (op, values) => LocalDateTimeFilter.call(op, values, createdAt) }
      }).toSet)

    val intersects = Seq(recipientSet, originatorSet, timeHorizonSet).flatten

    val messages = emptySeqAsOption(intersects).map(
      sets => msgRepository.get(sets.reduce(_.intersect(_)).toSeq)
    ).getOrElse(
      msgRepository.all()
    )

    emptySeqAsOption(messageFilters).map(messageFilters => {
      messages.filter(m => messageFilters.forall { case (op, values) =>
        StringFilter.call(op, values, m.message)
      })
    }).getOrElse(messages)
  }

  private def emptySeqAsOption[T](s: Seq[T]) = if (s.isEmpty) {
    None
  } else {
    Some(s)
  }

}
