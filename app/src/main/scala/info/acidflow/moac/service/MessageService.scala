package info.acidflow.moac.service

import info.acidflow.moac.model.{AggregateFunction, Message, Operator}

/**
  * Definition of a service layer handling messages.
  */
trait MessageService {

  /**
    * Save a message.
    * @param msg the message to save.
    */
  def save(msg: Message): Unit

  /**
    * Filters messages based on the filters passed to this function.
    * @param recipientsFilters
    * @param originatorFilter
    * @param createdAtFilter
    * @param messageFilters
    * @return
    */
  def filter(recipientsFilters: Seq[(Operator, String)], originatorFilter: Seq[(Operator, String)],
             createdAtFilter: Seq[(Operator, String)], messageFilters: Seq[(Operator, String)]): Iterable[Message]

  def aggregate(function: AggregateFunction, prop: Option[String], groupBy: Seq[String] = Seq.empty,
                recipientsFilters: Seq[(Operator, String)], originatorFilter: Seq[(Operator, String)],
                createdAtFilter: Seq[(Operator, String)],
                messageFilters: Seq[(Operator, String)]): Iterable[Map[String, Any]]
}
