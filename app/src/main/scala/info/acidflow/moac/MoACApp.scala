package info.acidflow.moac

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import info.acidflow.moac.json.{JsonSupport, UnmarshallerSupport}
import info.acidflow.moac.repository.message.InMemoryMessageRepository
import info.acidflow.moac.service.DefaultMessageService

import scala.io.StdIn
import scala.util.{Failure, Success}

object MoACApp extends App with JsonSupport with UnmarshallerSupport {

  // needed to run the route
  implicit val system = ActorSystem("message-over-avian-carrier")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  // Dependencies
  private val messagesRepository = new InMemoryMessageRepository()
  private val messageService = new DefaultMessageService(messagesRepository)
  private val messageController = new MessageController(messageService)

  val httpBindingFuture = Http().bindAndHandle(messageController.route, "localhost", 8080)
  httpBindingFuture.onComplete {
    case Success(binding) => println(
      s"""Server online at ${binding.localAddress.getHostString}:${binding.localAddress.getPort}.
         |Press RETURN to stop.
       """.stripMargin)
    case Failure(e) => println(s"An error occurred $e")
  }

  StdIn.readLine()

  httpBindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())

}
