package info.acidflow.moac.model

import java.time.LocalDateTime

object MessageAttrs {
  val recipient: String = "recipient"
  val originator: String = "originator"
  val createdAt: String = "createdAt"
  val message: String = "message"

  val All : Seq[String] = Seq(recipient, originator, createdAt, message)
}

case class Message(recipient: Long, originator: String, createdAt: LocalDateTime, message: String)
