package info.acidflow.moac.model

import java.time.LocalDateTime

object AggregateFunctions {

  def apply(name: String): AggregateFunction = name match {
    case "count" => Count
    case "min" => Min
    case "max" => Max
    case _ => throw new IllegalArgumentException(s"Unknown function $name")
  }

}

sealed trait AggregateFunction {

  val name: String

  def aggregate(props: Seq[String], prop: Option[String], msgs: Iterable[Message]): Iterable[Map[String, Any]] = {
    msgs.groupBy(msg => generateKey(props, msg))
      .map { case (k, v) => k + (name -> performAggregation(v, prop)) }
  }

  private def generateKey(props: Seq[String], msg: Message) = {
    if (props.isEmpty) {
      Map.empty[String, Any]
    } else {
      props.foldLeft(Map.empty[String, Any]) {
        case (current, prop) => prop match {
          case p@MessageAttrs.recipient => current + (p -> msg.recipient)
          case p@MessageAttrs.originator => current + (p -> msg.originator)
          case p@MessageAttrs.createdAt => current + (p -> msg.createdAt)
          case p@MessageAttrs.message => current + (p -> msg.message)
        }
      }
    }
  }

  private def performAggregation(msgs: Iterable[Message], prop: Option[String]) = {
    prop match {
      case Some(MessageAttrs.recipient) => aggLong(msgs.map(_.recipient))
      case Some(MessageAttrs.createdAt) => aggDate(msgs.map(_.createdAt))
      case Some(MessageAttrs.originator) => aggString(msgs.map(_.originator))
      case Some(MessageAttrs.message) => aggString(msgs.map(_.message))
      case None => aggLong(msgs.map(_.hashCode.toLong))
      case _ => ???
    }
  }

  def aggLong: Iterable[Long] => Any

  def aggString: Iterable[String] => Any

  def aggDate: Iterable[LocalDateTime] => Any
}


object Count extends AggregateFunction {

  override val name: String = "count"

  override def aggLong: Iterable[Long] => Any = seq => seq.size

  override def aggString: Iterable[String] => Any = seq => seq.size

  override def aggDate: Iterable[LocalDateTime] => Any = seq => seq.size
}

object Max extends AggregateFunction {

  override val name: String = "max"

  import info.acidflow.moac.order.DateOrdering.Implicits._

  override def aggLong: Iterable[Long] => Any = seq => seq.max

  override def aggString: Iterable[String] => Any = seq => seq.max

  override def aggDate: Iterable[LocalDateTime] => Any = seq => seq.max
}

object Min extends AggregateFunction {

  override val name: String = "min"

  import info.acidflow.moac.order.DateOrdering.Implicits._

  override def aggLong: Iterable[Long] => Any = seq => seq.min

  override def aggString: Iterable[String] => Any = seq => seq.min

  override def aggDate: Iterable[LocalDateTime] => Any = seq => seq.min

}