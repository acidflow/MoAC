package info.acidflow.moac.model

import java.time.format.DateTimeFormatter
import java.time.{LocalDate, LocalDateTime}

object Operators {

  def apply(name: String): Operator = name match {
    case "eq" => Equal
    case "ne" => NotEqual
    case "in" => In
    case "nin" => NotIn
    case "lt" => LowerThan
    case "lte" => LowerThanEqual
    case "gt" => GreaterThan
    case "gte" => GreaterThanEqual
    case "sw" => StartWith
    case "ew" => EndWith
    case "contains" => Contains
    case "regex" => Regex
    case _ => throw new IllegalArgumentException(s"Unknown operator $name")
  }

}

sealed trait Operator {
  def name: String
}

case object Equal extends Operator {
  val name = "eq"
}

case object NotEqual extends Operator {
  val name = "ne"
}

case object In extends Operator {
  val name = "in"
}

case object NotIn extends Operator {
  val name = "nin"
}

case object LowerThan extends Operator {
  val name = "lt"
}

case object LowerThanEqual extends Operator {
  val name = "lte"
}

case object GreaterThan extends Operator {
  val name = "gt"
}

case object GreaterThanEqual extends Operator {
  val name = "gte"
}

case object StartWith extends Operator {
  val name = "sw"
}

case object EndWith extends Operator {
  val name = "ew"
}

case object Contains extends Operator {
  val name = "contains"
}

case object Regex extends Operator {
  val name = "regex"
}

/**
  * Trait for common filtering operators.
  * @tparam T The type of the value to filter.
  */
trait Filter[T] {

  /**
    * Return true is value is equal to expected.
    */
  def eq(expected: T, value: T): Boolean = expected.equals(value)

  /**
    * Return true is value is not equal to expected.
    */
  def ne(expected: T, value: T): Boolean = !expected.equals(value)

  /**
    * Return true is value is in the expected sequence.
    */
  def in(expected: Seq[T], value: T): Boolean = expected.contains(value)

  /**
    * Return true is value is not in the expected sequence.
    */
  def nin(expected: Seq[T], value: T): Boolean = !expected.contains(value)

  /**
    * Return true is value is lower than expected.
    */
  def lt(expected: T, value: T)(implicit order: Ordering[T]): Boolean = order.lt(value, expected)

  /**
    * Return true is value is lower than or equal to expected.
    */
  def lte(expected: T, value: T)(implicit order: Ordering[T]): Boolean = order.lteq(value, expected)

  /**
    * Return true is value is greater than expected.
    */
  def gt(expected: T, value: T)(implicit order: Ordering[T]): Boolean = order.gt(value, expected)

  /**
    * Return true is value is greater than or equal to expected.
    */
  def gte(expected: T, value: T)(implicit order: Ordering[T]): Boolean = order.gteq(value, expected)

  /**
    * Convert a string representation of a value to the desired type T
    * @param str the string representation to convert from
    * @return the value of type T corresponding to this string representation
    */
  def convert(str: String): T

  private def convertSeq(str: String): Seq[T] = str.split(",").map(convert)

  def call(op: Operator, expected: String, value: T)(implicit order: Ordering[T]): Boolean = op match {
    case Equal => eq(convert(expected), value)
    case NotEqual => ne(convert(expected), value)
    case In => in(convertSeq(expected), value)
    case NotIn => nin(convertSeq(expected), value)
    case LowerThan => lt(convert(expected), value)
    case LowerThanEqual => lte(convert(expected), value)
    case GreaterThan => gt(convert(expected), value)
    case GreaterThanEqual => gte(convert(expected), value)
    case _ => throw new IllegalArgumentException(s"Unknown operator ${op.name}")
  }

}

object Filter {

  /**
    * Filter operating on strings.
    */
  implicit object StringFilter extends Filter[String] {
    override def convert(str: String): String = str

    /**
      * Returns true if value starts with expected.
      */
    def sw(expected: String, value: String): Boolean = value.startsWith(expected)

    /**
      * Returns true if value ends with expected.
      */
    def ew(expected: String, value: String): Boolean = value.endsWith(expected)

    /**
      * Returns true if value matches the given regular expression.
      */
    def regex(expected: String, value: String): Boolean = value.matches(expected)

    /**
      * Returns true if value contains expected.
      */
    def contains(expected: String, value: String): Boolean = value.contains(expected)

    override def call(op: Operator, expected: String, value: String)(implicit order: Ordering[String]): Boolean = op match {
      case StartWith => sw(convert(expected), value)
      case EndWith => ew(convert(expected), value)
      case Regex => regex(convert(expected), value)
      case Contains => contains(convert(expected), value)
      case _ => super.call(op, expected, value)
    }

  }

  /**
    * Filter operating on longs.
    */
  implicit object LongFilter extends Filter[Long] {
    override def convert(str: String): Long = str.toLong
  }

  /**
    * Filter operating on date time.
    */
  implicit object LocalDateTimeFilter extends Filter[LocalDateTime] {
    override def convert(str: String): LocalDateTime = LocalDateTime.parse(str, DateTimeFormatter.ISO_DATE_TIME)
  }

  /**
    * Filter operating on dates.
    */
  implicit object LocalDateFilter extends Filter[LocalDate] {
    override def convert(str: String): LocalDate = LocalDateTime.parse(str, DateTimeFormatter.ISO_DATE_TIME).toLocalDate
  }

}
