lazy val root = (project in file("."))
  .settings(
    scalaVersion := "2.12.6",
    version := "0.1",
    name := "MoAC"
  ).aggregate(app)

lazy val app = (project in file("app"))
  .settings(
    libraryDependencies ++= Dependencies.Modules.App
  )