import sbt._


object Dependencies {

  private object Version {
    val AkkaHttp = "10.1.3"
    val AkkaHttpSprayJson = "10.1.3"
    val AkkaStream = "2.5.14"
    val ScalaTests = "3.0.5"
    val ScalaMock = "4.1.0"
  }

  private object Library {
    val AkkaHttp = "com.typesafe.akka" %% "akka-http" % Version.AkkaHttp
    val AkkaHttpSprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % Version.AkkaHttpSprayJson
    val AkkaStream = "com.typesafe.akka" %% "akka-stream" % Version.AkkaStream
    val ScalaTests = "org.scalatest" %% "scalatest" % Version.ScalaTests
    val ScalaMock = "org.scalamock" %% "scalamock" % Version.ScalaMock
  }

  object Modules {
    val App = Seq(
      Library.AkkaHttp,
      Library.AkkaHttpSprayJson,
      Library.AkkaStream,
      Library.ScalaTests % Test,
      Library.ScalaMock % Test,

    )
  }

}
